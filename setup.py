#from setuptools import setup, find_packages
import setuptools

setuptools.setup(
    name='airport',
    version='0.0.1',
    url='https://airport.org',
    author='Anonymous',
    author_email='anonymous@example.org',
    packages=['airport']
)