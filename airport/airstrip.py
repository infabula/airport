class Airstrip:
    def __init__(self, name, length, width=20.0, max_weight=20.0):
        print("Airstrip Intializer")
        self.name = name
        self.length = length
        self.width = width
        self.max_weight = max_weight
        self.is_open = True
        self.is_occupied = False


    def set_length(self, length):
        if float(length) > 0.0:
            self._length = length
        else:
            raise ValueError("Length can't be negative")


    def get_length(self):
        return self._length

    length = property(get_length, set_length)
